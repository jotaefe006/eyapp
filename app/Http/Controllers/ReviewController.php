<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;
class ReviewController extends Controller
{

    public function index()
    {
        return view('layouts.list');
    }

    public function count()
    {
        $interview = Review::get();
        return $interview;
    }

    public function records(){

        $interview = Review::paginate(10);
        return [
            'pagination' => [
                'total' => $interview->total(),
                'current_page' => $interview->currentPage(),
                'per_page' => $interview->perPage(),
                'last_page' => $interview->lastPage(),
                'from' => $interview->firstItem(),
                'to' => $interview->lastItem(),
            ],
            'interview' => $interview
        ];
    }

    public function create(Request $request){
    
        try{

            $review = new Review();
            $review->name =  $request->name;
            $review->phone =  $request->phone;
            $review->email =  $request->email;
            $review->service =  $request->service;
            $review->proffesion =  $request->proffesion;
            $review->city =  $request->city;
            $review->save();
    
            return [
                'success' => true,
                'message' => "Gracias por registrarte"
            ];

        } catch (Exception $e){
            return [
                'error' => false,
                'message' => "Ocurrió un error en el registro"
            ];
        }  

    }
}


require('./bootstrap');
import Axios from 'axios'
window.Vue = require('vue');
import Vue from "vue";

// import VueConfirmDialog from "vue-confirm-dialog";

// Vue.use(VueConfirmDialog);

window.swal = require('./sweetalert2.all');

Vue.prototype.$eventHub = new Vue()
Vue.prototype.$http = Axios


Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('interview-component', require('./components/InterviewComponent.vue').default);
Vue.component('list-component', require('./components/ListComponent.vue').default);

const app = new Vue({
    el: '#app',
});

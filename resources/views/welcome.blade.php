<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/logo/logo.png">
  <title>
    ey!
  </title>
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
  <!-- Nucleo Icons -->
  <link href="../assets/css/nucleo-icons.css" rel="stylesheet" />
  <link href="../assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <link href="../assets/css/font-awesome.css" rel="stylesheet" />
  <link href="../assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- CSS Files -->
  <link href="../assets/css/argon-design-system.css?v=1.0.2" rel="stylesheet" />
</head>
<style >
    /* Extra small devices (phones, 600px and down) */
    @media only screen and (max-width: 600px) {
        .nav-ey{
            height:200px !important;
        }
    }

    /* Small devices (portrait tablets and large phones, 600px and up) */
    @media only screen and (min-width: 600px) {
        .nav-ey{
            height:200px !important;
        }
    }

    /* Medium devices (landscape tablets, 768px and up) */
    @media only screen and (min-width: 768px) {
        .nav-ey{
            height:320px !important;
        }
    }

    /* Large devices (laptops/desktops, 992px and up) */
    @media only screen and (min-width: 992px) {
        .nav-ey{
            height:350px;
        }
    }

    /* Extra large devices (large laptops and desktops, 1200px and up) */
    @media only screen and (min-width: 1200px) {
        .nav-ey{
            height:350px;
        }
    }
</style>

<body  class="profile-page">
  <!-- Navbar -->
  <nav id="navbar-main" class="navbar navbar-main navbar-expand-lg navbar-transparent headroom">
    <div class="container">
      <a class="navbar-brand mr-lg-5" href="../../../index.html">
        {{-- <img src="../assets/img/brand/white.png"> --}}
        ey! Socios
      </a>
      
      <div class="navbar-collapse collapse" id="navbar_global">
        <div class="navbar-collapse-header">
          <div class="row">
            <div class="col-6 collapse-brand">
              <a href="/">
                {{-- <img src="../assets/img/brand/blue.png"> --}}
              </a>
            </div>
            <div class="col-6 collapse-close">
              <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
                <span></span>
                <span></span>
              </button>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </nav>
  <!-- End Navbar -->
  <div id="app" class="wrapper">
    <section class="section-profile-cover section-shaped my-0 nav-ey">
      <!-- Circles background -->
      <img class="bg-image" src="../assets/img/pages/mohamed.jpg" style="width: 100%;"  >
      <!-- SVG separator -->
      <div class="separator separator-bottom separator-skew">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
          <polygon class="fill-secondary" points="2560 0 2560 100 0 100"></polygon>
        </svg>
      </div>
    </section>
    
    <section class="section bg-secondary">
      <div class="container">
        <div class="card card-profile shadow mt--300">
          <div class="px-4">
            <div class="row justify-content-center">
              <div class="col-lg-3 order-lg-2">
                <div class="card-profile-image">
                  <a href="javascript:;">
                    <img src="../assets/img/logo/ey.jpg" class="rounded-circle">
                  </a>
                </div>
              </div>
              <div class="col-lg-4 order-lg-3 text-lg-right align-self-lg-center">
                <div class="card-profile-actions py-4 mt-lg-0">

                </div>
              </div>
              <div class="col-lg-4 order-lg-1">
                
              </div>
            </div>

            <interview-component></interview-component>
           
          </div>
        </div>
      </div>
    </section>
    <footer class="footer">
      <div class="container">
        <div class="row row-grid align-items-center mb-5">
          <div class="col-lg-6">
            <h3 class="text-primary font-weight-light mb-2">Gracias por confiar en nosotros!</h3>
            <h4 class="mb-0 font-weight-light">Muy pronto estaremos en PlayStore y Appstore.</h4>
          </div>
          <div class="col-lg-6 text-lg-center btn-wrapper">
            <a target="_blank" href="https://www.facebook.com/Aplicacioney" rel="nofollow" class="btn-icon-only rounded-circle btn btn-facebook py-2" data-toggle="tooltip" data-original-title="Like us">
              <span class="btn-inner--icon"><i class="fa fa-facebook fa-3x"></i></span>
            </a>
            <a target="_blank" href="https://www.instagram.com/ey.app/?hl=es-la" rel="nofollow" class="btn btn-icon-only btn-dribbble rounded-circle py-2" data-toggle="tooltip" data-original-title="Follow us">
              <span class="btn-inner--icon"><i class="fa fa-instagram fa-3x"></i></span>
            </a>

          </div>
        </div>
        <hr>
        <div class="row align-items-center justify-content-md-between">
          <div class="col-md-6">
            <div class="copyright">
              &copy; 2020 <a href="http://ey-app.co" target="_blank">ey!</a>.
            </div>
          </div>
          <div class="col-md-6">
            
          </div>
        </div>
      </div>
    </footer>
  </div>
  <!--   Core JS Files   -->
  <script src="js/app.js"></script>
  <script src="js/plantilla.js"></script>
  <script src="../assets/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="../assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="../assets/js/core/bootstrap.min.js" type="text/javascript"></script>
  <script src="../assets/js/plugins/bootstrap-switch.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="../assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
  <!--  Plugin for the Carousel, full documentation here: http://jedrzejchalubek.com/ -->
  <script src="../assets/js/plugins/glide.js" type="text/javascript"></script>
  <!--  Plugin for the DatePicker, full documentation here: https://flatpickr.js.org/ -->
  <script src="../assets/js/plugins/moment.min.js"></script>
  <!--	Plugin for Select, full documentation here: https://joshuajohnson.co.uk/Choices/ -->
  <script src="../assets/js/plugins/choices.min.js" type="text/javascript"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://flatpickr.js.org/ -->
  <script src="../assets/js/plugins/datetimepicker.js" type="text/javascript"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="../assets/js/plugins/jasny-bootstrap.min.js"></script>
  <!-- Plugin for Headrom, full documentation here: https://wicky.nillia.ms/headroom.js/ -->
  <script src="../assets/js/plugins/headroom.min.js"></script>
  <!-- Control Center for Argon UI Kit: parallax effects, scripts for the example pages etc -->
</body>

</html>
